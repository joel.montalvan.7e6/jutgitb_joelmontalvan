val DifferentsProblems = listOf(
    Problem(
        "Pupitres",
        "En una escola tenim tres classes i volem saber quin és el nombre de taules que necessitarem tenir en total. " + "\n" +
                "Dependrà del nombre d'alumnes per aula. Cal tenir en compte que a cada taula hi caben 2 alumnes.",
        "Per l’entrada rebreu 3 enters que representen el nombre d’alumnes que hi ha a cada classe.",
        "La sortida ha de ser un enter que representi el nombre de pupitres necessaris.",
        "22 25 26",
        "37",
        "31 29 19",
        "41"
    ),
    Problem(
        "Calcula el descompte",
        "Llegeix el preu original i el preu actual i imprimeix el descompte (en %).",
        "Per l’entrada rebreu dos nombres decimals on, el primer, representa el preu original i, el segon, el preu amb descompte.",
        "Per la sortida sempre haureu d’imprimir un altre decimal amb el valor del descompte representat en %.",
        "1000 800",
        "20",
        "540 420",
        "22.22"
    ),
    Problem(
        "Quina és la mida de la meva pizza?",
        "Llegeix el diàmetre d'una pizza rodona i imprimeix la seva superfície. Pots usar Math.PI per escriure el valor de Pi.",
        "Per l’entrada rebreu un nombre decimal que representa el diàmetre d’una pizza.",
        "Per la sortida sempre haureu d’imprimir un altre decimal amb la superfície de la pizza.",
        "38",
        "1133.54",
        "55",
        "2375.83"
    ),
    Problem(
        "De Celsius a Fahrenheit",
        "Feu un programa que rebi una temperatura en graus Celsius i la converteixi en graus Fahrenheit",
        "Per l’entrada rebreu un nombre decimal que representa una temperatura en graus Celsius.",
        "Per la sortida sempre haureu d’imprimir un altre decimal amb el valor de la temperatura en graus Fahrenheit",
        "33.1",
        "91.58",
        "-3.2",
        "26.2"
    ),
    Problem(
        "Divisor de compte",
        "Fes un programa on, introduit el número de començals i el preu d'un sopar (que pot contenir cèntims), imprimeixi quan haurà de pagar cada començal.",
        "Per l’entrada rebreu un nombre enter amb el número de començals i un nombre decimal amb el cost del sopar.",
        "Per la sortida haureu d’imprimir quin és el cost del sopar per començal en Euros.",
        "4 76.56",
        "19.14€",
        "8 243.90",
        "30.4875€"
    )
)