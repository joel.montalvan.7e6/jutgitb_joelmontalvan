import java.util.Scanner

const val sinColor = "\u001b[0m"
const val negro = "\u001b[30m"
const val rojo = "\u001b[31m"
const val magenta = "\u001b[35m"
const val blanco = "\u001b[37m"
const val cyan = "\u001b[36m"
const val verde = "\u001b[32m"


class Problem(
    private val titolDelProblema:String,
    private val introDelProblema:String,
    private val input:String,
    private val output:String,
    private val inputPub:String,
    private val outputPub:String,
    private val inputPriv:String,
    private val outputPriv:String,
) {

    private var attempts = 0
    private var resolvedProblem = false

    fun questionProblem(){
        println(rojo + "Problema: ${this.titolDelProblema}"+ "\n")
        println(rojo + this.introDelProblema+ "\n" )
        println(this.input)
        println(this.output+ "\n" + sinColor)
        println(blanco + "Entrada Exemple: ${this.inputPub}")
        println(blanco +"Sortida Exemple: ${this.outputPub}"+ "\n" + sinColor)
        println(rojo+ "Entrada del programa a fer: ${this.inputPriv}"+ "\n"+sinColor)
    }
    fun attemptsStatus(){
        println(cyan + "Intents en total: ${this.attempts}")
        print(cyan + "Problema:")
        if(this.resolvedProblem != false){
            print("No Resolt"+ "\n" + sinColor)
        }
        else println("Resolt" + sinColor)
    }

    fun answerProblemResolved(){
        while (true){
            this.attempts++
            println(verde + "Quin es el resultat final?")
            val scanner = Scanner(System.`in`)
            val answer = scanner.next()
            if(answer == this.outputPriv) {
                println(verde + "El JUTG ho aprova \uD83D\uDC4D" + sinColor)
                this.attemptsStatus()
                this.resolvedProblem = true
                break
            }
            else println(verde + "El JUTG no ho aprova, continua intentant-ho \uD83D\uDC4E"+sinColor)
            this.attemptsStatus()
        }
    }
}
fun main(){
    println(verde + """
        ░░░░░██╗██╗░░░██╗████████╗░██████╗░
        ░░░░░██║██║░░░██║╚══██╔══╝██╔════╝░
        ░░░░░██║██║░░░██║░░░██║░░░██║░░██╗░     
        ██╗░░██║██║░░░██║░░░██║░░░██║░░╚██╗
        ╚█████╔╝╚██████╔╝░░░██║░░░╚██████╔╝
        ░╚════╝░░╚═════╝░░░░╚═╝░░░░╚═════╝░
    """.trimIndent() + "\n")

    println(verde + "Benvingut al JUTG del Institut Tenològic de Barcelona, preparat per resoldre tots el problemes proposats per mi?" + "\n" + "Doncs bona sort futur programador"+ "\n")

    println(cyan + """
        1. START
        2. EXIT
    """.trimIndent())

    menuDelPrograma()

    println("Espero que ho hagis disfrutat o... patit")

}

fun menuDelPrograma(){
    while (true){
        val scanner= Scanner(System.`in`)
        val opcionEscogida = scanner.nextInt()
        if (opcionEscogida == 1){
            iniciDelPrograma()
            break
        }
        if (opcionEscogida == 2){
            println(verde + "Fins aviat!!!")
            break
        }
        else println(cyan + "Aquesta opció no esta disponible:" + sinColor)
    }
}

fun iniciDelPrograma(){
    var resolvedProblem = 0

    for(i in DifferentsProblems.indices){
        val problem = DifferentsProblems[i]
        problem.questionProblem()
        println(verde + "Vols resoldre aquest problema?")
        println(verde + "INTRODUEIX (SI) O (NO):" + sinColor)

        while (true){
            val scanner = Scanner(System.`in`)
            val validateAnswer = scanner.next().uppercase()
            if(validateAnswer=="SI"){
                resolvedProblem++
                problem.answerProblemResolved()
                println(cyan + "Problemes fets per el moment: $resolvedProblem/5"+ "\n")
                break
            }
            else if(validateAnswer == "NO"){
                println(verde + "No passa res, ja ho faràs en un altre moment")
                println(verde + "Passem al següent problema" + sinColor)
                println(cyan + "Problemes fets per el moment: $resolvedProblem/5"+ "\n")
                break
            }
            else println(cyan + "El JUTG no ha no ha entès la teva resposta, torna a provar"+ "\n")
        }
    }
}

